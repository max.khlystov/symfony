window.AppTest = window.AppTest or {}
$ ->
  'use strict'
  window.AppTest.App = new (Marionette.Application)
  window.AppTest.App.addRegions
    'mainRegion': '#wrapper'
    'pagination': '#pagination-wrapper'
  window.AppTest.App.on 'start', ->
    Backbone.history.start()
    return
  window.AppTest.App.start()
  return
