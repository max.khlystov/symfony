window.AppTest = window.AppTest or {}
do ->
  window.AppTest.Router = new (Marionette.AppRouter)(
    controller: window.AppTest.Controller
    appRoutes:
      '': 'albumList'
      'album/:id': 'albumImages'
      'album/:id/page/:page': 'albumImages')
  return