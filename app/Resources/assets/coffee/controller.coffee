window.AppTest = window.AppTest or {}
do ->
  window.AppTest.Controller =
    albumList: ->
      albums = new (window.AppTest.AlbumCollection)
      albums.fetch success: ->
        console.log albums
        view = new (window.AppTest.AlbumListView)(collection: albums)
        window.AppTest.App.mainRegion.show view
        window.AppTest.App.getRegion('pagination').empty()
        return
      return
    albumImages: (id, page) ->
      console.log page
      images = new (window.AppTest.ImageCollection)({},
        albumId: id
        page: page)
      images.fetch success: ->
        view = new (window.AppTest.ImageListView)(collection: images)
        pagination = new (window.AppTest.PaginationView)(model: new (Backbone.Model)(
          data: images.paginationData
          albumId: id))
        window.AppTest.App.mainRegion.show view
        window.AppTest.App.pagination.show pagination
        return
      return
  return