window.AppTest = window.AppTest or {}
do ->
  window.AppTest.ImageItemView = Marionette.ItemView.extend(
    tagName: 'div'
    className: 'img-item'
    template: '#image-item-view')
  return
