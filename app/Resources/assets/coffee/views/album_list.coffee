window.AppTest = window.AppTest or {}
do ->
  window.AppTest.AlbumListView = Marionette.CollectionView.extend(
    tagName: 'div'
    className: 'row'
    childView: AppTest.AlbumItemView)
  return