window.AppTest = window.AppTest or {}
do ->
  window.AppTest.AlbumItemView = Marionette.ItemView.extend(
    tagName: 'div',
    className: 'album-item'
    template: '#album-item-view')
  return
