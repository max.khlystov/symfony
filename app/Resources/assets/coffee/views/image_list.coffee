window.AppTest = window.AppTest or {}
do ->
  window.AppTest.ImageListView = Marionette.CollectionView.extend(
    tagName: 'div'
    className: 'row'
    childView: AppTest.ImageItemView)
  return