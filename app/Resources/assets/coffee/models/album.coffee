window.AppTest = window.AppTest or {}
do ->
  window.AppTest.Album = Backbone.Model.extend(
    urlRoot: '/album'
    defaults: title: '')
  return