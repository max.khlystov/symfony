window.AppTest = window.AppTest or {}
do ->
  window.AppTest.ImageCollection = Backbone.Collection.extend(
    model: window.AppTest.Image
    urlRoot: '/album'
    parse: (data, options) ->
      @paginationData = data.pagination_data
      data.response
    initialize: (models, options) ->
      @albumId = options.albumId
      @page = options.page
      return
    url: ->
      console.log @page
      @urlRoot + '/' + @albumId + '/images?page=' + (if @page then @page else 1)
  )
  return