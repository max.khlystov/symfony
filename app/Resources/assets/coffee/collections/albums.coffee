window.AppTest = window.AppTest or {}
do ->
  window.AppTest.AlbumCollection = Backbone.Collection.extend(
    model: window.AppTest.Album
    url: '/api/album'
    parse: (data, options) ->
      data.response
  )
  return