var gulp = require('gulp');
var elixir = require('laravel-elixir');

 
elixir.config.assetsPath = 'app/Resources/assets/';
elixir.config.publicPath = 'web/assets/'

var scriptsPath = './app/Resources/assets/coffee/';
var stylesPath = './app/Resources/assets/css/';


var vendorPathes = {
  'jquery': './bower_components/jquery/dist/',
  'bootstrap': './bower_components/bootstrap/dist/',
  'underscore' : './bower_components/underscore/',
  'backbone' : './bower_components/backbone/',
  'marionette' : './bower_components/backbone.marionette/lib/',
};

var vendorScripts = [
  vendorPathes.jquery + 'jquery.min.js',
  vendorPathes.bootstrap + 'js/bootstrap.min.js',
  vendorPathes.underscore + 'underscore-min.js',
  vendorPathes.backbone + 'backbone-min.js',
  vendorPathes.marionette + 'backbone.marionette.min.js',
];

var appScripts = [
  scriptsPath + 'views/**',
  scriptsPath + 'models/**',
  scriptsPath + 'collections/**',
  
  scriptsPath + 'controller.coffee',
  scriptsPath + 'route.coffee',
  scriptsPath + 'main.coffee',
 
];

elixir(function(mix) {
  mix
    .scripts(vendorScripts, './web/assets/js/vendor.js', './')
    .copy(stylesPath + 'main.css', 'web/assets/css/app.css')
    .coffee(appScripts);
});