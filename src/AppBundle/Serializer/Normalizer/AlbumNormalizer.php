<?php

namespace AppBundle\Serializer\Normalizer;

use AppBundle\Entity\Album;
use AppBundle\Entity\Image;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

/**
 * Album normalizer
 */
class AlbumNormalizer extends SerializerAwareNormalizer implements NormalizerInterface
{
    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'id'     => $object->getId(),
            'title'  => $object->getTitle(),
            'images' => $this->serializeImages($object) 
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Album;
    }

    private function serializeImages($album)
    {
        $iamges = [];
        foreach ($album->getImages()->slice(0, 10) as $image) {
            $iamges[] = $this->serializer->normalize($image);
        }
        return $iamges;
    }

}