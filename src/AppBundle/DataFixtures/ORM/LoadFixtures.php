<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Album;
use AppBundle\Entity\Image;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Faker\Factory as Faker;

class LoadFixtures implements FixtureInterface, ContainerAwareInterface
{
    /** @var ContainerInterface */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $this->loadAlbums($manager);
    }

    private function loadAlbums(ObjectManager $manager)
    {
        $faker = Faker::create();
        

        for ($i = 0; $i < 5; $i++) {
            $album = new Album();
            $album->setTitle($faker->name);
            $manager->persist($album);
            
            for ($q = 0; $q < ($i == 0 ? 5 : rand(20, 25)); $q++) {
                $image = new Image();
                $image->setTitle($faker->name);
                $image->setPath($this->generateImage($faker));
                $image->setAlbum($album);

                $manager->persist($image);
            }
        }

        $manager->flush();
    }

    /**
     * Load and save random image
     *
     * @return string 
     */
    private function generateImage($faker)
    {   
        $imageDir = $this->container->get('kernel')->getRootDir().'/../web/uploads';
        $realPath = $faker->image($imageDir, $width = 640, $height = 480);

        return str_replace($imageDir.'/', '', $realPath);
    }

}