<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;

class ApiController extends Controller
{

    public function sendResponse($object, $paginationData = [])
    {
        $response = new JsonResponse();
        $serializer = $this->get('app.serializer.default');
 
        return $response->setData(array_merge(
            ['response' => $serializer->normalize($object)], 
            ['pagination_data' => $paginationData]
        ));
    }

}
