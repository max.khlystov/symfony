<?php

namespace AppBundle\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Controller\ApiController;
use Symfony\Component\HttpFoundation\Request;

class AlbumController extends ApiController
{
    /**
     * Get list of albums
     * @Route("/api/album", name="album_index")
     */
    public function indexAction(Request $request)
    {   
        $posts = $this->getDoctrine()
            ->getRepository('AppBundle:Album')->findAll();

        return $this->sendResponse($posts);
    }

    /**
     * Get list of images by album
     * @Route("/album/{id}/images ", requirements={"id": "[1-9]\d*"}, name="album_show")
     */
    public function albumImagesAction(Request $request, $id)
    {
        $images = $this->getDoctrine()
            ->getRepository('AppBundle:Image')
            ->createQueryBuilder('images')
        ->where('images.album_id = :album_id')
        ->setParameter('album_id', $id);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $images->getQuery(), 
            $request->query->getInt('page', 1), 10);

        return $this->sendResponse($pagination, $pagination->getPaginationData());
    }

}
