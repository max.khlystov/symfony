(function() {
  window.AppTest = window.AppTest || {};

  (function() {
    window.AppTest.AlbumItemView = Marionette.ItemView.extend({
      tagName: 'div',
      className: 'album-item',
      template: '#album-item-view'
    });
  })();

}).call(this);

(function() {
  window.AppTest = window.AppTest || {};

  (function() {
    window.AppTest.AlbumListView = Marionette.CollectionView.extend({
      tagName: 'div',
      className: 'row',
      childView: AppTest.AlbumItemView
    });
  })();

}).call(this);

(function() {
  window.AppTest = window.AppTest || {};

  (function() {
    window.AppTest.ImageItemView = Marionette.ItemView.extend({
      tagName: 'div',
      className: 'img-item',
      template: '#image-item-view'
    });
  })();

}).call(this);

(function() {
  window.AppTest = window.AppTest || {};

  (function() {
    window.AppTest.ImageListView = Marionette.CollectionView.extend({
      tagName: 'div',
      className: 'row',
      childView: AppTest.ImageItemView
    });
  })();

}).call(this);

(function() {
  window.AppTest = window.AppTest || {};

  (function() {
    window.AppTest.PaginationView = Marionette.ItemView.extend({
      template: '#pagination'
    });
  })();

}).call(this);

(function() {
  window.AppTest = window.AppTest || {};

  (function() {
    window.AppTest.Album = Backbone.Model.extend({
      urlRoot: '/album',
      defaults: {
        title: ''
      }
    });
  })();

}).call(this);

(function() {
  window.AppTest = window.AppTest || {};

  (function() {
    window.AppTest.Image = Backbone.Model.extend({
      defaults: {
        title: ''
      }
    });
  })();

}).call(this);

(function() {
  window.AppTest = window.AppTest || {};

  (function() {
    window.AppTest.AlbumCollection = Backbone.Collection.extend({
      model: window.AppTest.Album,
      url: '/api/album',
      parse: function(data, options) {
        return data.response;
      }
    });
  })();

}).call(this);

(function() {
  window.AppTest = window.AppTest || {};

  (function() {
    window.AppTest.ImageCollection = Backbone.Collection.extend({
      model: window.AppTest.Image,
      urlRoot: '/album',
      parse: function(data, options) {
        this.paginationData = data.pagination_data;
        return data.response;
      },
      initialize: function(models, options) {
        this.albumId = options.albumId;
        this.page = options.page;
      },
      url: function() {
        console.log(this.page);
        return this.urlRoot + '/' + this.albumId + '/images?page=' + (this.page ? this.page : 1);
      }
    });
  })();

}).call(this);

(function() {
  window.AppTest = window.AppTest || {};

  (function() {
    window.AppTest.Controller = {
      albumList: function() {
        var albums;
        albums = new window.AppTest.AlbumCollection;
        albums.fetch({
          success: function() {
            var view;
            console.log(albums);
            view = new window.AppTest.AlbumListView({
              collection: albums
            });
            window.AppTest.App.mainRegion.show(view);
            window.AppTest.App.getRegion('pagination').empty();
          }
        });
      },
      albumImages: function(id, page) {
        var images;
        console.log(page);
        images = new window.AppTest.ImageCollection({}, {
          albumId: id,
          page: page
        });
        images.fetch({
          success: function() {
            var pagination, view;
            view = new window.AppTest.ImageListView({
              collection: images
            });
            pagination = new window.AppTest.PaginationView({
              model: new Backbone.Model({
                data: images.paginationData,
                albumId: id
              })
            });
            window.AppTest.App.mainRegion.show(view);
            window.AppTest.App.pagination.show(pagination);
          }
        });
      }
    };
  })();

}).call(this);

(function() {
  window.AppTest = window.AppTest || {};

  (function() {
    window.AppTest.Router = new Marionette.AppRouter({
      controller: window.AppTest.Controller,
      appRoutes: {
        '': 'albumList',
        'album/:id': 'albumImages',
        'album/:id/page/:page': 'albumImages'
      }
    });
  })();

}).call(this);

(function() {
  window.AppTest = window.AppTest || {};

  $(function() {
    'use strict';
    window.AppTest.App = new Marionette.Application;
    window.AppTest.App.addRegions({
      'mainRegion': '#wrapper',
      'pagination': '#pagination-wrapper'
    });
    window.AppTest.App.on('start', function() {
      Backbone.history.start();
    });
    window.AppTest.App.start();
  });

}).call(this);

//# sourceMappingURL=app.js.map
